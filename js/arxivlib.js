/**
 * Created by bbaer on 11/15/18.
 */

function ArxivPublications( query, displayTemplate, target ) {
    this.query = query;
    this.displayTemplate = displayTemplate;
    this.target = target;


    this.prototype.formatDate = function(date) {
        var pubDate = date;

        var pubDay = pubDate.substr(6, 2);
        var pubMonth = getMonth(pubDate.substr(4, 2));
        var pubYear = pubDate.substr(0, 4);

        pubDate = pubDay + ". " + pubMonth + " " + pubYear;

        if (pubDay === "00" && pubMonth === "00") {
            pubDate = pubYear;
        } else if (pubDay === "00") {
            pubDate = pubMonth + " " + pubYear;
        }
        return pubDate;
    };

    this.prototype.getMonth = function(month) {
        switch (month) {
            case "01":
                month = "January";
                break;
            case "02":
                month = "February";
                break;
            case "03":
                month = "March";
                break;
            case "04":
                month = "April";
                break;
            case "05":
                month = "May";
                break;
            case "06":
                month = "June";
                break;
            case "07":
                month = "July";
                break;
            case "08":
                month = "August";
                break;
            case "09":
                month = "September";
                break;
            case "10":
                month = "October";
                break;
            case "11":
                month = "November";
                break;
            case "12":
                month = "December";
                break;
        }
        return month;
    }

    this.prototype.replaceTemplateVariables = function(publications) {
        var exp = {
            summary : /(\(\(summary\)\))/g,
            link       : /(\(\(link\)\))/g,
            authorsBreak   : /(\(\(authorsBreak\)\))/g,
            authorsComma   : /(\(\(authorsComma\)\))/g,
            title    : /(\(\(title\)\))/g,
            updated     : /(\(\(updatedDate\)\))/g,
            published  : /(\(\(publicationDate\)\))/g,
        };

        var string = this.displayTemplate;
        string = string.replace(exp.summary, publications.summary);
        string = string.replace(exp.link , publications.link);
        string = string.replace(exp.authorsBreak, this.createAuthorString(publications.authors, "Break"));
        string = string.replace(exp.authorsComma, this.createAuthorString(publications.authors, "Comma"));
        string = string.replace(exp.updated, publications.updated);
        string = string.replace(exp.title, publications.title);
        string = string.replace(exp.published, publications.published);
        return string;
    };

    this.prototype.createAuthorString = function(authors, seperator) {
        var text = "";
        var sep = "";
        switch(seperator) {
            case "Break":
              sep = "<br>";
              break;
            case "Comma":
              sep = ", ";
              break;
            default:
              console.log("No known seperator for authors defined.");
        }

        for(var i = 0; authors.length < i; i++) {
            if (i > 0) { text += sep; }
            text += authors[i];
        }
        return txt;
    };

    this.prototype.getPublications = function() {
        var that = this;
        console.log("Get Data");
        $.get(this.query, function (data) {
            var $xml = $(data);

            $xml.find("entry").each(function () {
                var $this = $(this),
                    publications = {
                        title: $this.find("title").text(),
                        authors: [],
                        link: $this.find("id").text(),
                        summary: $this.find("summary").text(),
                        published: $this.find("published").text(),
                        updated: $this.find("type").text()
                    };

                $xml.find("author").each(function () {
                    publications.authors.push($(this).text());
                });

                publications.published = that.formatDate(publications.publicated);
                publications.updated = that.formatDate(publications.updated);

                var arxivFormatted = replaceTemplateVariables(publications);

                $(that.target).append(arxivFormatted);
            });
        });
    };
}
